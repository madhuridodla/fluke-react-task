import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import Footer from '../components/footer';
import Header from '../components/header';
import { commonApiCall } from '../utlis'
import BeatLoader from "react-spinners/BeatLoader";

const Categories = ({ commonApiCall, ...props }) => {
  const [categories, setCategories] = useState([])
  const [loader, setLoader] = useState(true)
  useEffect(() => {
    GetCategories()
  }, [])
  const GetCategories = async () => {
    let response = await commonApiCall('/categories', '', 'get', "CATEGORIES")
    if (response && response.status == 200) {
      setCategories(response.data)
      setLoader(false)
    }
  }

  return (
    <React.Fragment>
      <Header />
      <div className="container">
        <div className="row categories">
          {
            loader ? <div className="loader-css"><BeatLoader size={15} margin={2} loading={loader} /></div> :
              categories && categories.length && Array.isArray(categories) ? categories.map((cat, key) => {
                return (
                  <div className="card" key={key}>
                    <img src={cat.image_uri ? cat.image_uri : "https://react.semantic-ui.com/images/wireframe/image.png"} alt={cat.display_name} style={{ width: '100%' }} />
                    <ul className="breadcrumb">
                      <li><a href="#">{cat.category_name}</a></li>
                      <li>{cat.display_name}</li>
                    </ul>
                    <h3>{cat.display_name ? cat.display_name : "Title"}</h3>
                    <p className="price">$19.99</p>
                    <p>{cat.description ? cat.description : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."}</p>
                    <p><button>Add to Cart</button></p>
                  </div>
                )
              })
                : "No Categories Found"
          }
        </div>
      </div>
      <Footer />
    </React.Fragment>
  )
}
export default connect(null, { commonApiCall })(Categories)