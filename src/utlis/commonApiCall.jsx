import axios from "axios";
import { API_URL } from "../config/configs";

import { showWarnToast, showErrorToast, getItem } from "../utlis/commonFunctions";
import { validationMessages } from "../redux/actions";

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    // console.log("errr", error);
    if (error && error.response && 400 === error.response.status) {
      if (error.response !== "") return Promise.resolve(error.response);
    }
    if (error && error.response && 404 === error.response.status) {
      if (error.response !== "") return Promise.resolve(error.response);
    } else if (error && error.response && 403 === error.response.status) {
      showWarnToast(validationMessages.SEND_PROPER_DATA);
    } else if (error && error.response && 500 === error.response.status) {
      showErrorToast(validationMessages.INTERNAL_SERVER_ERROR);
    } else if (error && error.response && 401 === error.response.status) {
      showErrorToast(validationMessages.SESSTION_EXPIRED);
      localStorage.clear();
      window.location.href = "/login";
    } else {
      return Promise.reject(error);
    }
  }
);
/*******************************************************************************************
     @PURPOSE      	: 	Call api,
     @Parameters 	: 	{
       url : <url of api>
       data : <data object (JSON)>
       method : String (get, post)
       isForm (Optional) : Boolean - to call api with form data
       isPublic (Optional) : Boolean - to call api without auth header
     }
     /*****************************************************************************************/
export const commonApiCall = (
  url,
  data,
  method,
  Type = null,
  isAuthorized = false,
  isForm = false,
) => (dispatch) => {

  url = API_URL + url;
  let headers = { "Content-Type": "application/json" },
    token = getItem("token") ? getItem("token") : null;
  if (isAuthorized) headers.Authorization = token;
  if (isForm) headers["Content-Type"] = "multipart/form-data";
  // Api calling
  return new Promise((resolve, reject) => {
    axios({ method, url, headers, data })
      .then((response) => {
        // console.log('response', response)
        if ((response && response.status === 200) || (response && response.status === 201)) {
          if (Type !== null) {
            var payload = { data: response.data };
            dispatch({ type: Type, payload });
          }
          let Data = { data: response.data, status: response.status };
          resolve(Data);
        } else if (response && response.status === 404) {
          let Data = { data: response.data, status: response.status };
          resolve(Data);
        } else if (response && response.status === 400) {
          let Data = { data: response.data, status: response.status };
          resolve(Data);
        } else {
          // typeof (data.message ? data.message : 'error') === "string" ? common.showErrorToast(data.message ? data.message : 'error') : common.showWarnToast("Something went wrong! ")
        }
      })
      .catch((error) => {
        // this.showErrorToast(error);
        console.log("error is ", error);
        reject(error);
      });
  });
};

//To identify the plan
export const AgentPlan = (data) => async (dispatch) => {
  dispatch({ type: "AGENT_PLAN", payload: data });
};
