import { toast } from "react-toastify";
toast.configure();
/*************************************************************************************
                    @PURPOSE  	:  Email Validation
                    @Parameters : 	{
                      email:email(value)
                      }
**************************************************************************************/
export const validateEmail = (email) => {
  let pattern = new RegExp(
    /^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
  return pattern.test(email);
};
//***********************************************************************************//

/*************************************************************************************
                      @PURPOSE      	:    Password Validation
                      @Parameters 	: 	{
                      pass:value
                      }
**************************************************************************************/
export const validatePassword = (pass) => {
  // let pattern = new RegExp(/^(?=.*[a-z])(?=.*[0-9])/);
  let pattern = pass.length >= 6 ? true : false;
  return pattern;
};
//***********************************************************************************//

/*************************************************************************************
                           @PURPOSE  	:  Validate Mobile number
                    @Parameters : 	{
                       mobileNo:value
                      }  
**************************************************************************************/
export const validateMobileNumber = (mobileNo) => {
  // var pattern = new RegExp(/^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/);
  var pattern = new RegExp(/^[0][1-9]\d{9}$|^[1-9]\d{9}$/);
  // return pattern.test(mobileNo);
  return pattern.test(mobileNo);
};
//***********************************************************************************//

/*************************************************************************************
                              @PURPOSE  	:  Set success Toast Message
                    @Parameters : 	{
                       msg:value
                      }  
**************************************************************************************/
export const showSuccessToast = (msg) => {
  toast.success(msg, {
    position: "top-right",
    autoClose: 3000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
  });
};
//***********************************************************************************//

/*************************************************************************************
                     @PURPOSE  	:   Set Error Toast Message
                    @Parameters : 	{
                       msg:value
                      } 
**************************************************************************************/
export const showErrorToast = (msg) => {
  toast.error(msg, {
    position: "top-right",
    autoClose: 4000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
  });
};
//***********************************************************************************//
export const getItem = (key) => {
  return localStorage.getItem(key);
};

/*************************************************************************************
                    @PURPOSE  	:   Set WARNING Toast Message
                    @Parameters : 	{
                       msg:value
                      } 
  **************************************************************************************/
export const showWarnToast = (msg) => {
  toast.warn(msg, {
    position: "top-right",
    autoClose: 4000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
  });
};
/*************************************************************************************/
