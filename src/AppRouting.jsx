import React, { lazy, Suspense } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { BasicsPagesRoutes } from './utlis'
const Categories = lazy(() => import("./screens/Categories"));



function AppRouting() {
  return (
    <React.Fragment>
      <BrowserRouter>
        <Switch>
          <Suspense fallback={<div></div>}>
            {/* Navigation of pages */}
            <Route
              exact
              path={BasicsPagesRoutes.CATEGORIES}
              component={Categories}
            />

          </Suspense>
          {/* End Navigation of pages */}
        </Switch>
      </BrowserRouter>
    </React.Fragment>
  )
}
export default AppRouting