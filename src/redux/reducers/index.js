import { combineReducers } from 'redux';
import Reducer from './Reducer';

// Combine all reducers into root reducer
export default combineReducers({
    reducer: Reducer
});