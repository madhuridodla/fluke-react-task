const INTIAL_STATE = {
    loginData: {},
}
export default (state = INTIAL_STATE, action) => {
    switch (action.type) {
        case "LOGIN":
            return Object.assign({}, state, { loginData: action.payload.data })

        case "LOGOUT":
            return INTIAL_STATE
        default:
            return state;
    }

}