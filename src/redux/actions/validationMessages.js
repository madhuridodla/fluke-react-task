// *************************************************************//
//@Purpose : We can use following function to use localstorage
//*************************************************************//
export const validationMessages = {
  PROVIDE_FIRST_NAME: "* Please enter firstname",
  PROVIDE_LAST_NAME: "* Please enter lastname",
  PROVIDE_EMAIL: "* Please enter email",
  PROVIDE_VALID_EMAIL: "* Please enter valid email",
  PROVIDE_MOBILE: "* Please enter mobile number",
  PROVIDE_VALID_MOBILE: "* Please enter valid mobile number of 10 digits",
  PROVIDE_PASSWORD: "* Please enter password",
  PROVIDE_VALID_PASSWORD: "* Please enter valid password of length 6"
};
