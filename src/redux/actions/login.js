import { LOGIN, LOGOUT } from './types'

export const LoginActionCreator = (data) => async dispatch => {
  dispatch({ type: LOGIN, payload: data });
};

export const Logout = () => async dispatch => {
  dispatch({ type: LOGOUT, payload: '' });
};


