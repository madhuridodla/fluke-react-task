import React from 'react';

const Header = () => {

  function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }
  return (
    <div className="topnav" id="myTopnav">
      <a href="#home" className="active">Home</a>
      <a href="#news">News</a>
      <a href="#contact">Contact</a>
      <a href="#about">About</a>
      <input type="text" placeholder="Search.." />
      <a params="[object Object]" className="icon" onClick={myFunction}>
        <i className="fa fa-bars" />
      </a>
    </div>

  )
}
export default Header