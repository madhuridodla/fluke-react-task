import React from 'react';
const Footer = () => {
  return (
    <footer>
      <div className="container">
        <div className="footer-end">
          {/* <div className="footer-logo">
          <a title="Paradox" href="#">
            logo
          </a>
        </div> */}
        </div>
        <div className="copyright-part text-center">
          <p>© 2020 All rights reserved.</p>
          <ul>
            <li><a title="Privacy Policy" href="#">Privacy Policy</a></li>
            <li><a title="Terms of Use" href="#">Terms of Use</a></li>
          </ul>
        </div>
      </div>
    </footer>
  )
}
export default Footer
