const express = require('express');
const path = require('path');
const app = express();
// import { PORT } from "./src/config/configs";
const PORT = 4000

app.use(express.static(path.join(__dirname, 'build')));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(PORT);
console.log(`listening on port ${PORT}.....`)