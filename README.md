# HELLO STAR

## Prerequisite (Development)

| Module | Version |
| ------ | ------- |
| Node   | 12.14.0 |
| Npm    | 6.13.4  |
| React  | 16.13.1 |
| Redux  | 4.0.5   |

---

## Running Project In Local

```bash
$ git clone -b <branchName> <gitUrl> <projectName>

$ cd configs

$ cp configSample.js configs.js

$ vi configs.js (need to change environment)

$ npm install

$ npm start

```

---

## Running Project on server

```bash

$ node server.js

$ Running on port 4000

```

---

## Running unit tests

> Run `npm run test` to execute the unit tests via [Jest](https://jestjs.io/docs/en/tutorial-react).

---

## Deployment In Staging Server

```bash
$ git clone -b <branchName> <gitUrl> <projectName>

$ cd configs

$ mv configSample.js configs.js

$ vi configs.js (need to change environment)

$ npm run build

"build" folder will be created.

Open Server & run start pm2 instance:

$ pm2 start server.js --name="<instance_name>"

```

---

## Start Server Using pm2

```bash
$ pm2 start server.js --name="<instance_name>" (for creating)

$ pm2 restart <instance_id> or <instance_name>

$ pm2 delete <instance_id> or <instance_name>

$ pm2 logs <instance_id> or <instance_id> (to check logs or errors)

$ pm2 logs <instance_id> --lines=100 (to check particular lines of previous logs)

```

---

## Deployment With CI/CD

> Coming Soon

---

## React Coding Standards

> You can get more information on React coding stantdards from [here](https://drive.google.com/file/d/1kJ7ab6B4KEkul23pjkMZpbqucyWGGgf_/view?usp=sharing).

---

## Directory Structure

```
React_Seed_Admin_v16.8.6/
|-- .scannerwork
|-- nodemodules/
|-- public/
|-- src/
|-- redux
    |-- actions/
        |-- commonFunctions.js
        |-- index.js
        |-- reducers/
        |-- adminReducer.js
        |-- index.js
    |-- store/
    |-- components/
    |-- configs/
        |-- configs.js
        |-- configSample.js
    |-- Screens/

  |-- Utlis/
  |-- App.css
  |-- App.jsx
  |-- App.test.js
  |-- index.js
  |-- serviceWorker.js
|-- .env
|-- .gitignore
|-- package.json
|-- README.md
|-- server.js

```

---
